ansible
ansible-lint
flake8
molecule==5.0.1 # W/A for https://github.com/ansible-community/molecule/issues/3903
molecule-libvirt
libvirt-python
pykickstart
yamllint
